#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <time.h>
#include "Header.h"

int main()
{
	srand((unsigned)time(NULL));
	char mop[4] = { 'w','s','d','a' };
	int cod;
	while (1) {
		cod = meni(mop[0], mop[1]);
		if (cod == 4) { options(mop, mop[0], mop[1]); }
		if (cod == 1) { play(mop[0], mop[1], mop[2], mop[3]); }
		if (cod == 2) { scoreboard(); }
		if (cod == 3) { gameplay(); }
	}
	return 0;
}